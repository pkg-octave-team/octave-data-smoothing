octave-data-smoothing (1.3.0-9) unstable; urgency=medium

  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * Set upstream metadata fields: Archive.
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 04:22:53 -0300

octave-data-smoothing (1.3.0-8) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Tue, 28 Jul 2020 04:15:04 -0300

octave-data-smoothing (1.3.0-7) unstable; urgency=medium

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

  [ Rafael Laboissiere ]
  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:55:09 -0200

octave-data-smoothing (1.3.0-6) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:28:08 -0200

octave-data-smoothing (1.3.0-5) unstable; urgency=medium

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

  [ Rafael Laboissiere ]
  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
  * d/control:
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Use secure URIs in the Vcs-* fields
    + Use cgit instead of gitweb in Vcs-Browser URL
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:19 -0200

octave-data-smoothing (1.3.0-4) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 26 Jul 2015 23:16:39 +0200

octave-data-smoothing (1.3.0-3) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * debian/watch: Use the SourceForge redirector
  * Remove obsolete DM-Upload-Allowed flag

  [ Sébastien Villemot ]
  * Add Rafael Laboissiere and Mike Miller to Uploaders.
  * Bump to Standards-Version 3.9.6, no changes needed.
  * Use my @debian.org email address

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 18 Sep 2014 17:14:41 +0200

octave-data-smoothing (1.3.0-2) unstable; urgency=low

  * debian/patches/autoload-yes.patch: new patch

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Thu, 22 Mar 2012 19:30:53 +0000

octave-data-smoothing (1.3.0-1) unstable; urgency=low

  * Imported Upstream version 1.3.0
  * Bump to debhelper compat level 9
  * debian/copyright: upgrade to machine-readable format 1.0
  * Bump to Standards-Version 3.9.3, no changes needed
  * Build-depend on octave-pkg-dev >= 1.0.1, to compile against Octave 3.6
  * Remove ${shlibs:Depends}, meaningless on an arch:all package

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sat, 17 Mar 2012 15:34:54 +0100

octave-data-smoothing (1.2.3-1) unstable; urgency=low

  [ Sébastien Villemot ]
  * Imported Upstream version 1.2.3 (Closes: #651064)
  * Bump to debhelper compat level 8
  * Bump Standards-Version to 3.9.2, no changes needed
  * Add myself to Uploaders
  * debian/copyright: upgrade to latest DEP5

  [ Thomas Weber ]
  * Switch architecture to "all", the package contains only .m files.
  * Remove debian/clean file; the current package doesn't have any of the
    files mentioned there.

 -- Thomas Weber <tweber@debian.org>  Sat, 10 Dec 2011 19:35:33 +0100

octave-data-smoothing (1.2.0-3) unstable; urgency=low

  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571839)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Update copyright information
  * Bump standards version to 3.9.1, no changes needed
  * Switch to dpkg-source 3.0 (quilt) format

 -- Thomas Weber <tweber@debian.org>  Wed, 23 Feb 2011 23:08:06 +0100

octave-data-smoothing (1.2.0-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Build-depend on octave-pkg-dev >= 0.7.0, such that the
    package is built against octave3.2

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 29 Nov 2009 16:51:55 +0100

octave-data-smoothing (1.2.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
  * debian/copyright:
    + Use DEP5 URL in Format-Specification
    + Use separate License stanzas for instructing about the location of
      the different licenses used in the package

 -- Rafael Laboissiere <rafael@debian.org>  Sun, 24 May 2009 10:53:29 +0200

octave-data-smoothing (1.1.1-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 29 Mar 2009 00:33:09 +0100

octave-data-smoothing (1.1.1-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * debian/control: Bump Standards-Version to 3.8.0 (no changes needed)
  * debian/rules: Use debian/clean instead of manually cleaning files
  * debian/compat, debian/control: Bump build-dependency on debhelper to
    >= 7.0.0, otherwise debian/clean is moot
  * debian/clean: New file

  [ Ólafur Jens Sigurðsson ]
  * debian/watch: Fixed the path to the filename

  [ Thomas Weber ]
  * New upstream release
  * Bump dependency on octave-pkg-dev to 0.6.1, to get the experimental
    version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Tue, 02 Dec 2008 21:26:25 +0100

octave-data-smoothing (1.0.0-1) unstable; urgency=low

  [ Olafur Jens Sigurdsson ]
  * Initial release. (Closes: #480765)

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 21 May 2008 07:04:50 +0000
